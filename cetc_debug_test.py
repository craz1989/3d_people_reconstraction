mport os
import cv2
import shutil
root_old_dir="D:\\work\\3D_project\\3d_recostraction\\dataset\\my\\my_1"
root_new_dir="D:\\work\\3D_project\\3d_recostraction\\dataset\\my\\my_1_sample"
file_path="D:\work\\3D_project\\3d_recostraction\\dataset\\my\\my_1\\idx.txt"

def getFileNameAndCameraIdx(path):
    file_path, file_name=os.path.split(path)
    file_path, camera_idx=os.path.split(file_path)
    print(f"path->{file_path}, file_name->{file_name}, camera_idx->{camera_idx}")
    return file_name, camera_idx

file=open(file_path,"r")
files=file.readlines()
rects_path=os.path.join(root_new_dir,"rects")
images_path=os.path.join(root_new_dir,"images")
masks_path=os.path.join(root_new_dir,"masks")
humans_path=os.path.join(root_new_dir,"humans")
os.makedirs(rects_path,exist_ok=True)
os.makedirs(images_path,exist_ok=True)
os.makedirs(masks_path,exist_ok=True)
os.makedirs(humans_path,exist_ok=True)
for f in files:
    if f[-1]=='\n':
        f=f[:-1]
    file_name, _=getFileNameAndCameraIdx(f)
    print(f"path->{f}")
    shutil.copy(f,os.path.join(humans_path,file_name))
    shutil.copy(os.path.join(root_old_dir,'images',file_name),os.path.join(images_path,file_name))
    shutil.copy(os.path.join(root_old_dir, 'mask',  file_name),
                os.path.join(masks_path,  file_name))
    image_id,_=os.path.splitext(file_name)
    rect_filename=image_id+"_rect.txt"
    shutil.copy(os.path.join(root_old_dir, 'rects', rect_filename),
                os.path.join(rects_path, rect_filename))
    # shutil.copy(os.path.join(root_old_dir,camera_idx,images_path))

    # break
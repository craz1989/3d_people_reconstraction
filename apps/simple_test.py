# Copyright (c) Facebook, Inc. and its affiliates. All rights reserved.


from recon import reconWrapper
import argparse
import  os
from datetime import datetime
import  shutil
import cv2
import numpy as np
###############################################################################################
##                   Setting
###############################################################################################
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input_path', type=str, default='../sample_images')
parser.add_argument('-o', '--out_path', type=str, default='../results')
parser.add_argument('-c', '--ckpt_path', type=str, default='../scripts/checkpoints/pifuhd.pt')
parser.add_argument('-r', '--resolution', type=int, default=128)
parser.add_argument('--use_rect', action='store_true', help='use rectangle for cropping')
parser.add_argument("--use_description",type=bool,default=True)
parser.add_argument("--images_dir",type=str,required=False)
parser.add_argument("--rects_dir",type=str, required=False)
args = parser.parse_args()
###############################################################################################
##                   Upper PIFu
###############################################################################################

resolution = str(args.resolution)

# CETC ADD new directory for even experimental versions
# BEGIN
args.images_dir="D:\\work\\3D_project\\3d_recostraction\\dataset\\my\\my_1_sample\\humans"
args.rects_dir="D:\\work\\3D_project\\3d_recostraction\\dataset\\my\\my_1_sample\\rects"

def addNoise(image):
    mean=0
    sigma=1000**0.5
    gaussian = np.random.normal(mean, sigma, (image.shape[0], image.shape[1],image.shape[2]))
    noisy_img=image+gaussian
    cv2.normalize(noisy_img, noisy_img, 0, 255, cv2.NORM_MINMAX, dtype=-1)
    noisy_img = noisy_img.astype(np.uint8)
    return noisy_img

def getTimeAsString():
    curent_time=datetime.now()
    time_as_string=str(curent_time.year)+"_"+str(curent_time.month)+"_"+str(curent_time.day)+"_"+str(curent_time.hour)+"_"+str(curent_time.minute)
    return time_as_string
def addDarckness(image):
    hsv=cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
    hsv=hsv.astype('int32')
    hsv[...,2]-=40
    hsv[hsv > 255] = 250
    hsv[hsv <0 ] = 5
    hsv = hsv.astype('uint8')
    hsv = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    #
    # cv2.imshow("s",image)
    # cv2.imshow("d",hsv)
    # cv2.waitKey()
    return  hsv
args.out_path=os.path.join(args.input_path,getTimeAsString())
args.input_path=args.out_path
os.makedirs(args.input_path,exist_ok=True)
add_noise=False
add_darkness=True
for file in os.listdir(args.images_dir):
    if add_noise:
        image=cv2.imread(os.path.join(args.images_dir,file))
        image=addNoise(image)
        cv2.imwrite(os.path.join(args.input_path,file),image)
        continue
    if add_darkness:
        image = cv2.imread(os.path.join(args.images_dir, file))
        image = addDarckness(image)
        cv2.imwrite(os.path.join(args.input_path, file), image)
        continue
    shutil.copy(os.path.join(args.images_dir,file),os.path.join(args.input_path,file))
for file in os.listdir(args.rects_dir):
    if os.path.isfile(os.path.join(args.rects_dir,file)):
        rect_path=os.path.join(args.rects_dir,file)
        rect_path_new=os.path.join(args.input_path,file)
        f=open(rect_path,"r")
        x,y,w,h=[int(i)for i in f.read().split()]
        dp=100
        x=max(0,x-dp)
        y=max(0,y-dp)
        w+=dp*2
        h+=dp*2
        f.close()
        f=open(rect_path_new,"w")
        f.write(" ".join([str(i) for i in  (x,y,w,h)]))
        # shutil.copy(os.path.join(args.rects_dir,file),os.path.join(args.input_path,file))


# END

start_id = -1
end_id = -1
cmd = ['--dataroot', args.input_path, '--results_path', args.out_path,\
       '--loadSize', '1024', '--resolution', resolution, '--load_netMR_checkpoint_path', \
       args.ckpt_path,\
       '--start_id', '%d' % start_id, '--end_id', '%d' % end_id]
reconWrapper(cmd, args.use_rect)

# '{file_name}_rect.txt", containing (left, top, width, height).